{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE TemplateHaskell #-}

module PersistTacToe.Types where

import Data.Aeson.TH (defaultOptions, deriveJSON)
import Database.Persist.Sql

data Player = Naught | Cross deriving (Eq, Show)
newtype Board = Board [Maybe Player] deriving (Eq, Show)
data Result = Won Player | Draw deriving (Eq, Show)

emptyBoard :: Board
emptyBoard = Board $ replicate 9 Nothing

instance PersistField Board where
    toPersistValue   = toPersistValueJSON
    fromPersistValue = fromPersistValueJSON

instance PersistFieldSql Board where
    sqlType _ = SqlBlob

instance PersistField Result where
    toPersistValue   = toPersistValueJSON
    fromPersistValue = fromPersistValueJSON

instance PersistFieldSql Result where
    sqlType _ = SqlBlob

$(deriveJSON defaultOptions ''Player)
$(deriveJSON defaultOptions ''Board)
$(deriveJSON defaultOptions ''Result)
