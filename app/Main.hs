module Main where

import PersistTacToe (run)

import System.Environment (getArgs)

main :: IO ()
main = run =<< getArgs
